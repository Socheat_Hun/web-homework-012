
import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Menu from './components/Menu';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from './components/Home'
import Video from './components/Video'
import Account from './components/Account'
import Auth from './components/Auth'
import RouterReact from './components/RouterReact';
import Error from './components/Error'
import VCard from './components/VCard'

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      DataCard: [{
        id: 1,
        title: 'TOM AND JERRY ',
        description: 'This website stores cookies on your computer. These cookies are used to collect information about how you interact with our website and allow us to remember you about content for all.',
        img: 'https://cdn.images.express.co.uk/img/dynamic/41/590x/Tom-and-Jerry-518252.jpg'

      },
      {
        id: 2,
        title: 'TOM AND JERRY',
        description: 'This website stores cookies on your computer. These cookies are used to collect information about how you interact with our website and allow us to remember you about content for all.',
        img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTiOi0kpCIK0Dte7FcW02v5NXvrNqRVwsmgdDlunx67m_n8LLkC&usqp=CAU'

      },
      {
        id: 3,
        title: 'TOM AND JERRY',
        description: 'This website stores cookies on your computer. These cookies are used to collect information about how you interact with our website and allow us to remember you about content for all.',
        img: 'https://i.ytimg.com/vi/b41pA0yFADg/maxresdefault.jpg'

      },
      {
        id: 4,
        title: 'TOM AND JERRY',
        description: 'This website stores cookies on your computer. These cookies are used to collect information about how you interact with our website and allow us to remember you about content for all.',
        img: 'https://img.republicworld.com/republic-prod/stories/promolarge/xxhdpi/1wdx67zlxacqbseu_1587398388.jpeg?tr=w-1242,h-710,f-jpeg'

      },
      {
        id: 5,
        title: 'TOM AND JERRY',
        description: 'This website stores cookies on your computer. These cookies are used to collect information about how you interact with our website and allow us to remember you about content for all.',
        img: 'https://i.ytimg.com/vi/X12CPlgDLPg/maxresdefault.jpg'

      },
      {
        id: 6,
        title: 'TOM AND JERRY',
        description: 'This website stores cookies on your computer. These cookies are used to collect information about how you interact with our website and allow us to remember you about content for all.',
        img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQwsytwj43Q670YmuaAxc7SdbNQ1Qia-VIXeap3Hc2QzGWCqvM0&usqp=CAU'

      }]
    }

  }
  render() {
    return (
      <div className="App">
        <Router>
          <Menu />
          <Switch>
            <Route path='/' exact component={RouterReact} />
            <Route path='/Home' render={() => <Home DataCard={this.state.DataCard} />} />
            <Route path='/Video' component={Video} />
            <Route path='/Account' component={Account} />
            <Route path='/Auth' component={Auth} />
            <Route path='/VCard/:id' render={(props) => <VCard {...props} DataCard={this.state.DataCard} />}/>
            <Route path='*' component={Error} />
            
          </Switch>
        </Router>
      </div>
    );
  }
}
