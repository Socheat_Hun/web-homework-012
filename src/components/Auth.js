import React from 'react'
import { Form, InputGroup, Button, FormControl } from 'react-bootstrap'
function Auth() {
    return (
        <div className="container box">
            <Form inline>
                <Form.Control
                    className="mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Username" />
                <InputGroup className="mb-2 mr-sm-2">
                    <Form.Control type="password" placeholder="Password" />
                </InputGroup>
                <Button type="submit" className="mb-2" variant="dark">Submit</Button>
            </Form>
        </div>
    )
}
export default Auth
