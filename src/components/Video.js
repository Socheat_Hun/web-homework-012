import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
const routes = [
    {
        path: "/Movies",
        component: Movies,
        routes: [
            {
                path: "/Movies/Cartoon",
                component: Cartoon
            },
            {
                path: "/Movies/Adventure",
                component: Adventure
            },
            {
                path: "/Movies/Hollywood",
                component: Hollywood
            },
            {
                path: "/Movies/Horror",
                component: Horror
            }
        ]
    },
    {
        path: "/Animation",
        component: Animation,
        routes: [
            {
                path: "/Animation/Action",
                component: Action
            },
            {
                path: "/Animation/Romance",
                component: Romance
            },
            {
                path: "/Animation/Comedy",
                component: Comedy
            }
        ]
    }
];
export default function RouteConfigExample() {
    return (
        <div className="container video">
            <Router>
                <div>
                    <ul>
                        <li>
                            <Link to="/Animation">Animation</Link>
                        </li>
                        <li>
                            <Link to="/Movies">Movies</Link>
                        </li>
                    </ul>
                    <Switch>
                        {routes.map((route, i) => (
                            <RouteWithSubRoutes key={i} {...route} />
                        ))}
                        <h3>Please Select A Topic</h3>
                    </Switch>
                </div>
            </Router>
        </div>
    );
}
function RouteWithSubRoutes(route) {
    return (
        <Route
            path={route.path}
            render={props => (
                <route.component {...props} routes={route.routes} />
            )}
        />
    );
}

function Movies({ routes }) {
    return (<div>
        <h2>Movies Category</h2>
        <ul>
            <li>
                <Link to="/Movies/Cartoon">Cartoon</Link>
            </li>
            <li>
                <Link to="/Movies/Adventure">Adventure</Link>
            </li>
            <li>
                <Link to="/Movies/Hollywood">Hollywood</Link>
            </li>
            <li>
                <Link to="/Movies/Horror">Horror</Link>
            </li>
        </ul>

        <Switch>
            {routes.map((route, i) => (
                <RouteWithSubRoutes key={i} {...route} />
            ))}
            <h3>Please Select A Topic</h3>
        </Switch>
    </div>);
}

function Animation({ routes }) {
    return (
        <div>
            <h2>Animation Category</h2>
            <ul>
                <li>
                    <Link to="/Animation/Action">Action</Link>
                </li>
                <li>
                    <Link to="/Animation/Romance">Romance</Link>
                </li>
                <li>
                    <Link to="/Animation/Comedy">Comedy</Link>
                </li>
            </ul>

            <Switch>
                {routes.map((route, i) => (
                    <RouteWithSubRoutes key={i} {...route} />
                ))}
                <h3>Please Select A Topic</h3>
            </Switch>
        </div>

    );
}

function Action() {
    return <h3>Action</h3>;
}

function Romance() {
    return <h3>Romance</h3>;
}
function Comedy() {
    return <h3>Comedy</h3>;
}

function Cartoon() {
    return <h3>Cartoon</h3>;
}

function Adventure() {
    return <h3>Adventure</h3>;
}
function Hollywood() {
    return <h3>Hollywood</h3>;
}
function Horror() {
    return <h3>Horror</h3>;
}

