import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

export default function Account() {
    return (
        <Router>
            <div className="container video">
                <h2>Account</h2>
                <ul>
                    <li><Link to="/Netflix">Netflix</Link></li>
                    <li><Link to="/Zillow">Zillow Group</Link></li>
                    <li><Link to="/Yahoo">Yahoo</Link></li>
                    <li><Link to="/Modus">Modus Create</Link></li>
                </ul>
                <hr />
                <Switch>
                    <Route exact path="/Netflix"><Netflix/></Route>
                    <Route path="/Zillow"><Zillow/></Route>
                    <Route path="/Yahoo"><Yahoo /></Route>
                    <Route path="/Modus"><Modus /></Route>
                </Switch>
            </div>
        </Router>
    );
}
function Netflix() {
    return (
        <div>
            <h3>The <span className="color">name</span> in the query string is "Netflix"</h3>
        </div>
    );
}

function Zillow() {
    return (
        <div>
            <h3>The <span className="color">name</span> in the query string is "Zillow Group"</h3>
        </div>
    );
}

function Yahoo() {
    return (
        <div>
            <h3>The <span className="color">name</span> in the query string is "Yahoo"</h3>
        </div>
    );
}
function Modus() {
    return (
        <div>
            <h3>The <span className="color">name</span> in the query string is "Modus Create"</h3>
        </div>
    );
}