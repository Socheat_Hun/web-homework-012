import React from 'react'
import { CardDeck, Card } from 'react-bootstrap'
import { Link } from 'react-router-dom'
function Home({ DataCard }) {

    let showCard = DataCard.map((d) =>
        <CardDeck key={d.id} className="col-md-4 size">
            <Card>
                <Card.Img variant="top" src={d.img} />
                <Card.Body>
                    <Card.Title>{d.title}</Card.Title>
                    <Card.Text>
                        {d.description}
                    </Card.Text>
                    <Card.Link as={Link} to={`/VCard/${d.id}`}>See More...</Card.Link>
                </Card.Body>
                <Card.Footer>
                        <small className="text-muted">Posted By Socheat Hun</small>
                </Card.Footer>
            </Card>
        </CardDeck>
    )
    return (
        <div className="container demention">
            <div className="row">
                {showCard}
            </div>
        </div>
    )
}

export default Home

