import React from 'react'
import { Navbar, Nav,Form,Button,FormControl } from 'react-bootstrap'
import {Link} from 'react-router-dom'
function Menu() {
  return (
    <div>
        <Navbar bg="dark" variant="dark">
          <div className= "container">
          <Navbar.Brand as ={Link} to ='/'>React Router</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link as ={Link} to= '/Home' >Home</Nav.Link>
            <Nav.Link as ={ Link} to= '/Video'>Video</Nav.Link>
            <Nav.Link as ={Link} to= '/Account'>Account</Nav.Link>
            <Nav.Link as ={ Link} to='/Auth'>Auth</Nav.Link>

          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-info">Search</Button>
          </Form>
          </div>
        </Navbar>  
    </div>

  )
}

export default Menu
