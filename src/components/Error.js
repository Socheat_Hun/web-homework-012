import React from 'react'

function Error() {
    return (
        <div>
            <h3> This Page Not Found </h3>
        </div>
    )
}

export default Error
